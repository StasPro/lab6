
<?php get_header( 'header.php'); ?>
	<section class="billboard light">
		<header class="wrapper light">
			<a href="#"><img class="logo" src="<?php the_field('logo_firm_l','2'); ?>" alt=""/></a>
			<nav>
				<ul>
					<li><a href=""><?php the_field('above_link1','2'); ?></a></li>
					<li><a href=""><?php the_field('above_link2','2'); ?></a></li>
					<li><a href=""><?php the_field('above_link3','2'); ?></a></li>
					<li><a href=""><?php the_field('above_link4','2'); ?></a></li>
				</ul>
			</nav>
		</header>

		<div class="caption light animated wow fadeInDown clearfix">
			<h1><?php the_field('main_title','2'); ?></h1>
			<p><?php the_field('text_bot_mtitle','2'); ?></p>
			<hr>
		</div>
		<div class="shadow"></div>
	</section><!--  End billboard  -->


	<section class="services wrapper">
		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<img class="icon" src="<?php the_field('img_merit1','2'); ?>" alt=""/>
				<span class="separator"></span>
				<h2><?php the_field('head_merit1','2'); ?></h2>
				<p><?php the_field('text_merit1','2'); ?></p>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<img class="icon" src="<?php the_field('img_merit2','2'); ?>" alt=""/>
				<span class="separator"></span>
				<h2><?php the_field('head_merit2','2'); ?></h2>
				<p><?php the_field('text_merit2','2'); ?></p>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<img class="icon" src="<?php the_field('img_merit3','2'); ?>" alt=""/>
				<span class="separator"></span>
				<h2><?php the_field('head_merit3','2'); ?></h2>
				<p><?php the_field('text_merit3','2'); ?></p>
			</li>
		</ul>
	</section><!--  End services  -->


	<section class="video">
		<img src="<?php the_field('big_img','2'); ?>" alt="" class="video_logo animated wow fadeInDown"/>
		<h3 class="animated wow fadeInDown"><?php the_field('text_big_img','2'); ?></h3>
		<!-- <a href="http://www.youtube.com/embed/cBJyo0tgLnw" id="play_btn" class="fancybox animated wow flipInX" data-wow-duration="2s"></a> -->
	</section><!--  End video  -->


	<section class="testimonials wrapper">
		<div class="title animated wow fadeIn">
			<h2><?php the_field('head_com','2'); ?></h2>
			<h3><?php the_field('text_com','2'); ?></h3>
			<hr class="separator"/>
		</div>

		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<p><img src="<?php the_field('quotes','2'); ?>" alt="" class="quotes"/><?php the_field('client1_com','2'); ?>
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="<?php the_field('client1_img','2'); ?>" class="avatar"/>
					<div class="client_details">
						<h4><?php the_field('client1_name','2'); ?></h4>
						<h5><?php the_field('client1_work','2'); ?></h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<p><img src="<?php the_field('quotes','2'); ?>" alt="" class="quotes"/><?php the_field('client2_com','2'); ?>
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="<?php the_field('client2_img','2'); ?>" class="avatar"/>
					<div class="client_details">
						<h4><?php the_field('client2_name','2'); ?></h4>
						<h5><?php the_field('client2_work','2'); ?></h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<p><img src="<?php the_field('quotes','2'); ?>" alt="" class="quotes"/><?php the_field('client3_com','2'); ?>
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="<?php the_field('client3_img','2'); ?>" class="avatar"/>
					<div class="client_details">
						<h4><?php the_field('client3_name','2'); ?></h4>
						<h5><?php the_field('client3_work','2'); ?></h5>
					</div>
				</div>
			</li>
		</ul>
	</section><!--  End testimonials  -->


	<section class="blog_posts">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<h2><?php the_field('head_posts','2'); ?></h2>
				<h3><?php the_field('head_posts_text','2'); ?></h3>
				<hr class="separator"/>
			</div>

			<ul class="clearfix">
				<li class="animated wow fadeInDown">
					<div class="media">
						<div class="date">
							<span class="day"><?php the_field('post1_day','2'); ?></span>
							<span class="month"><?php the_field('post1_month','2'); ?></span>
						</div>
						<a href="#">
							<img src="<?php the_field('post1_img','2'); ?>" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1><?php the_field('post1_text','2'); ?></h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<div class="date">
							<span class="day"><?php the_field('post2_day','2'); ?></span>
							<span class="month"><?php the_field('post2_month','2'); ?></span>
						</div>
						<a href="#">
							<img src="<?php the_field('post2_img','2'); ?>" alt=""/>
						</a>
					</div>					
					<a href="#">
						<h1><?php the_field('post2_text','2'); ?></h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".4s">
					<div class="media">
						<div class="date">
							<span class="day"><?php the_field('post3_day','2'); ?></span>
							<span class="month"><?php the_field('post3_month','2'); ?></span>
						</div>
						<a href="#">
							<img src="<?php the_field('post3_img','2'); ?>" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1><?php the_field('post3_text','2'); ?></h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".6s">
					<div class="media">
						<div class="date">
							<span class="day"><?php the_field('post4_day','2'); ?></span>
							<span class="month"><?php the_field('post4_month','2'); ?></span>
						</div>
						<a href="#">
							<img src="<?php the_field('post4_img','2'); ?>" alt=""/>
						</a>
					</div>
					<a href="#"><h1><?php the_field('post4_text','2'); ?></h1>
				</a>
				</li>
			</ul>
		</div>
	</section><!--  End blog_posts  -->


	<footer>
		<div class="wrapper">
			<div class="rights">
				<img src="img/footer_logo.png" alt="" class="footer_logo"/>
				<p><?php the_field('foot_about_firm','2'); ?><a href="http://pixelhint.com" target="_blank"><?php the_field('foot_site','2'); ?></a></p>
			</div>

			<nav>
				<ul>
					<li><a href="#"><?php the_field('foot_about','2'); ?></a></li>
					<li><a href="#"><?php the_field('foot_faq','2'); ?></a></li>
					<li><a href="#"><?php the_field('foot_serv','2'); ?></a></li>
					<li><a href="#"><?php the_field('foot_blog','2'); ?></a></li>
					<li><a href="#"><?php the_field('foot_cont','2'); ?></a></li>
				</ul>
			</nav>
		</div>		
	</footer><!--  End footer  -->
<?php get_footer( 'footer.php'); ?>	
    